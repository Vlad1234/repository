package com.app.Repository;

import org.springframework.data.repository.CrudRepository;
import com.app.Domain.Hotel;

public interface HotelRepository extends CrudRepository<Hotel, Integer>
{
}


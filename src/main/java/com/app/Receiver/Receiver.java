package com.app.Receiver;

import com.app.Domain.Hotel;
import com.app.Sender.Sender;
import com.app.Service.HotelService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import static java.lang.Integer.parseInt;

@RabbitListener(queues = "c2r")
public class Receiver {

    //autowired the StudentService class
    @Autowired
    HotelService hotelService;

    @Autowired
    Hotel hotel;

    @Autowired
    Sender sender;

    @RabbitHandler
    public void receive(String message) {
        String[] buffer = message.split("-");

        hotel.setId(parseInt(buffer[0]));
        hotel.setRoom(parseInt(buffer[1]));
        hotel.setName(buffer[2]);
        hotel.setEmail(buffer[3]);

        hotelService.saveOrUpdate(hotel);
        System.out.println(" [x] Received '" +
                hotel.getId()+"-"+
                hotel.getRoom()+"-"+
                hotel.getName()+"-"+
                hotel.getEmail()+"'");

        sender.send("created field with {" +
                "id: " + hotel.getId()+
                ", room: " + hotel.getRoom()+
                ", name: " + hotel.getName()+
                ", email: " + hotel.getEmail());
    }
}

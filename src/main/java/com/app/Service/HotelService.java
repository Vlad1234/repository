package com.app.Service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.Domain.Hotel;
import com.app.Repository.HotelRepository;
//defining the business logic
@Service
public class HotelService
{
    @Autowired
    HotelRepository hotelRepository;
    //getting all student records
    public List<Hotel> getAllStudent()
    {
        List<Hotel> hotels = new ArrayList<Hotel>();
        hotelRepository.findAll().forEach(student -> hotels.add(student));
        return hotels;
    }
    //getting a specific record
    public Hotel getStudentById(int id)
    {
        return hotelRepository.findById(id).get();
    }
    public void saveOrUpdate(Hotel student)
    {
        hotelRepository.save(student);
    }
    //deleting a specific record
    public void delete(int id)
    {
        hotelRepository.deleteById(id);
    }
}